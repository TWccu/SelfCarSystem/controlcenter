import json
import random
import socket
import threading
import time

import paho.mqtt.client as mqtt

from led_controller import SevenSegmentDisplay
from light_sensor import Photoresistor

one = 4
two = 6
#lightSensor1 = Photoresistor(22)
lightSensor1 = Photoresistor(29)
lightSensor2 = Photoresistor(31)
sevenSegmentDisplay0 = SevenSegmentDisplay(11, 12, 13)
sevenSegmentDisplay1 = SevenSegmentDisplay(15, 16, 18)
sevenSegmentDisplay2 = SevenSegmentDisplay(36, 38, 40)
doorTable = {"1": random.randint(one, two), "2": random.randint(one, two)}
#newNum = random.randint(1, 9)
while doorTable["1"] == doorTable["2"]:
    doorTable["2"] = random.randint(one, two)
centerSevenSegmentDisplay = [doorTable["1"], doorTable["2"]]
sevenSegmentDisplay0.display(0)
sevenSegmentDisplay1.display(centerSevenSegmentDisplay[0])
sevenSegmentDisplay2.display(centerSevenSegmentDisplay[1])
originLightCounter = 35
lightCounter = originLightCounter
print("lC: ", lightCounter)

with open('selfCarConfig.json') as f:
    data = json.load(f)
    mqtt_in_topic = data["mqtt_in_topic"]
    print("mqtt_in_topic: ", mqtt_in_topic)
    mqtt_out_topic = data["mqtt_out_topic"]
    print("mqtt_out_topic: ", mqtt_out_topic)
    # mqtt_broker = data["mqtt_broker"]
    # print("mqtt_broker: ", mqtt_broker)
    # mqtt_port = data["mqtt_port"]
    # print("mqtt_port: ", mqtt_port)
    lightThreshold = data["lightThreshold"]
    print("lightThreshold: ", lightThreshold)

client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
client.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
client.bind(("", 23130))
data, addr = client.recvfrom(1024)
payload = json.loads(data.decode('utf-8'))
mqtt_broker = payload['mqttip']
mqtt_port = payload['mqttport']
print('MQTT Server ' + mqtt_broker + ':' + str(mqtt_port))


def resetSevenSegmentDisplay():
    global centerSevenSegmentDisplay
    global doorTable
    global sevenSegmentDisplay1
    global sevenSegmentDisplay2
    print(doorTable)
    centerSevenSegmentDisplay = [doorTable["1"], doorTable["2"]]
    print(centerSevenSegmentDisplay)
    i = random.randint(0, 1)
    if i == 1:
        a = centerSevenSegmentDisplay[0]
        centerSevenSegmentDisplay[0] = centerSevenSegmentDisplay[1]
        centerSevenSegmentDisplay[1] = a
    sevenSegmentDisplay1.display(centerSevenSegmentDisplay[0])
    sevenSegmentDisplay2.display(centerSevenSegmentDisplay[1])
    state_center(1)
    time.sleep(1)
    state_center(2)
    time.sleep(1)

def on_connect(client, userData, flags, rc):
    print("Connected with result code " + str(rc))
    publish_mqtt("1," + str(doorTable["1"]))
    publish_mqtt("2," + str(doorTable["2"]))
    client.subscribe(mqtt_in_topic)
    centerThread = threading.Thread(target=controller_center)
    centerThread.start()


def publish_mqtt(msgs):
    client.publish(mqtt_out_topic, msgs)


def split_message(msgs):
    return msgs.split(',')


def state_center(doorNum):
    global doorTable
    msgs = str(doorNum) + "," + str(doorTable[str(doorNum)])
    publish_mqtt(msgs)


def on_message(client, userData, msg):
    msgs = msg.payload.decode("utf8")
    msgs_array = split_message(msgs)
    if msgs_array[0] == 'state':  ## state,doorNum
        state_center(msgs_array[1])


def openDoor(doorNum):
    print("openDoor")
    print(doorTable)
    print(centerSevenSegmentDisplay)
    if doorTable["1"] == doorNum:
        publish_mqtt("open,1")  ## open,1
    elif doorTable["2"] == doorNum:
        publish_mqtt("open,2")


def closeDoor(doorNum):
    print("closeDoor")
    print(doorTable)
    print(centerSevenSegmentDisplay)
    if doorTable["1"] == doorNum:
        publish_mqtt("close,1")  ## close,1
    elif doorTable["2"] == doorNum:
        publish_mqtt("close,2")


def controller_center():
    global centerSevenSegmentDisplay
    global doorTable
    global lightCounter
    while True:
        if lightSensor1.isLight(lightThreshold):
            print("1: ", lightCounter)
            lightCounter -= 1
            if lightCounter <= -5:
                openDoor(centerSevenSegmentDisplay[0])
                time.sleep(2)
                while lightSensor1.isLight(lightThreshold):
                    continue
                lightCounter = originLightCounter
                time.sleep(8)
                closeDoor(centerSevenSegmentDisplay[0])
                newNum = random.randint(one, two)
                while checkRepeat(2, newNum):
                    newNum = random.randint(one, two)
                doorTable["1"] = newNum
                resetSevenSegmentDisplay()
        elif lightSensor2.isLight(lightThreshold):
            print("2: ", lightCounter)
            lightCounter -= 1
            if lightCounter <= 0:
                openDoor(centerSevenSegmentDisplay[1])
                time.sleep(2)
                while lightSensor2.isLight(lightThreshold):
                    continue
                lightCounter = originLightCounter
                time.sleep(8)
                closeDoor(centerSevenSegmentDisplay[1])
                newNum = random.randint(one, two)
                while checkRepeat(1, newNum):
                    newNum = random.randint(one, two)
                doorTable["2"] = newNum
                resetSevenSegmentDisplay()
        else:
            lightCounter = originLightCounter


def checkRepeat(index, num):
    if doorTable[str(index)] == num:
        return True
    else:
        return False


if __name__ == '__main__':
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(mqtt_broker, mqtt_port, 60)
    client.loop_forever()
